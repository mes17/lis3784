--1.
    --old style join --ERROR 1054 (42S22): Unknown column 'aut' in 'field list'
    select mem_fname, mem_lname, b.bok_isbn, bok_title, lon_loan_date, lon_due_date, aut_fname, aut,lname
    from member m, loaner l, book b, attribution at, author a
    where m.mem_id=l.mem_id
    and l.bok_isbn=b.bok_isbn
    and b.bok_isbn=at.bok_isbn
    and at.aut_id=a.aut_id
    order by lon_due_date desc;

    --join on -- ERROR 1054 (42S22): Unknown column 'aut_lname' in 'field list'
    select mem_fname, mem_lname, b.bok_isbn, bok_title, lon_loan_date, lon_due_date, aut_fname, aut_lname
    from member m 
    join loaner l on m.mem_id=l.mem_id
    join book b on l.bok_isbn=b.bok_isbn
    join attribution at on b.bok_isbn=at.bok_isbn
    join author a on at.aut_id=a.aut_id
    order by lon_due_date desc;

    --join on -- took from canvas quiz A3
    select mem_fname, mem_lname, lon_loan_date, lon_due_date, b.bok_isbn, bok_title, aut_fname, aut_lname
    from member m
    join loaner l on m.mem_id=l.mem_id
    join book b on l.bok_isbn=b.bok_isbn
    join attribution at on b.bok_isbn=at.bok_isbn
    join author a on at.aut_id=a.aut_id
    order by lon_due_date desc;

    --join using -- ERROR 1054 (42S22): Unknown column 'bok_isbn' in 'from clause'
    select mem_fname, mem_lname, lon_loan_date, lon_due_date, bok_isbn, bok_title, aut_fname, aut_lname
    from member
    join loaner using(mem_id)
    join book using(bok_isbn)
    join attribution using(bok_isbn)
    join author using(aut_id)
    order by lon_due_date desc;

    -- join using-- no copy
    select mem_fname, mem_lanme, lon_loan_date, lone_due_date, bok_isbn, bok_title, aut_fnmae, aut_lname
    from member
    join loaner using (mem_id)
    join book using (bok_isbn)
    join attribution using (bok_isbn)
    join author using (aut_id)
    order by lon_due_date desc;

    --natural join -- ERROR 1054 (42S22): Unknown column 'aut_lname' in 'field list'
    select mem_fname, mem_lname, lon_loan_date, lon_due_date, bok_isbn, bok_title, aut_fname, aut_lname
    from member
    natural join loaner
    natural join book
    natural join attribution
    natural join author
    order by lon_due_date desc;

--2.
    --step #1
    select bok_price, bok_price * .85,format(bok_price*.85,2)
    from book;

    --step #2: add dollar sign ($), and alias
    select concat('$',format(bok_price*.85,2)) as book_sale_price
    from book;

    --step #2: alias (with space): back ticks
    select concat('$',format(bok_price*.85,2)) as 'book_sale_price'
    from book;

    --step #2: or, alias (with space): single quotation marks, and 5 decimal places
    select concat('$',format(bok_price*.85,5)) as 'book_sale_price'
    from book;

    --step #2: or, alias(with space): double quotation marks, and 5 decimal places
    select concat('$',format(bok_price*.85,5)) as "book_sale_price"
    from book;

--3.
select*from member;

    --step #1:
    select bok_price,bok_price*.85
    from book
    where bok_isbn='1234567890345';
    
    --step #2
    select concat('Purchased book at discounted price:','$',format(bok_price*.85,2))
    from book
    where bok_isbn='1234567890345';

    --step #3
    --check data
    select*from member;

    update member 
    set mem_notes =
    (
        select concat('Purchased book at discounted price:','$',format(bok_price*.85,2))
        from book
        where bok_isbn='1234567890345'
    )
    where mem_id = 3;

    select*from member;


--4. 
drop table if exists test;
create table if not exists test
(
tst_id int unsigned NOT NULL AUTO_INCREMENT,
tst_fname varchar(15) NOT NULL,
tst_lname varchar(30) NOT NULL,
tst_street varchar(30) NOT NULL,
tst_city varchar(20) NOT NULL,
tst_state char(2) NOT NULL default 'FL',
tst_zip int unsigned NOT NULL,
tst_phone bigint unsigned  NOT NULL COMMENT 'otherwise, cannot make contact',
tst_email varchar(100) DEFAULT NULL,
tst_notes varchar(255) DEFAULT NULL,
PRIMARY KEY (tst_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;

--5. 
select * from test;

insert into test
(tst_id,tst_fname,tst_lname,tst_street,tst_city,tst_state,tst_zip,tst_phone,tst_email,tst_notes)
select mem_id,mem_fname,mem_lname,mem_street,mem_city,mem_state,mem_zip,mem_phone,mem_email,mem_notes
from member;

select * from test;

--6.
describe test;

ALTER TABLE test change tst_lname tst_last varchar(35) not null DEFAULT 'Doe' COMMENT 'testing';

describe test;