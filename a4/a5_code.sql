SET ANSI_WARNINGS ON;
GO

use master;
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name =N'mes17')
DROP DATABASE mes17;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name =N'mes17')
CREATE DATABASE mes17;
GO

use mes17;
GO


IF OBJECT_ID (N'dbo.applicant',N'U') IS NOT NULL
DROP TABLE dbo.applicant;
GO

CREATE TABLE dbo.applicant
(
    app_id SMALLINT not null identity(1,1),
    app_ssn int NOT NULL check (app_ssn > 0 and app_ssn <=999999999),
    app_state_id VARCHAR(45) NOT NULL,
    app_fname VARCHAR(15) NOT NULL,
    app_lname VARCHAR(30) NOT NULL,
    app_street VARCHAR(30) NOT NULL,
    app_city VARCHAR(30) NOT NULL,
    app_state CHAR(2) NOT NULL DEFAULT 'FL',
    app_zip int NOT NULL check (app_zip > 0 and app_zip <= 999999999),
    app_email VARCAHR(100) NULL,
    app_dob DATE NOT NULL,
    app_gender CHAR(1) NOT NULL CHECK (app_gender IN('m','f')),
    app_bckgd_check CHAR(1) NOT NULL CHECK (app_bckgd_check IN('n','y')),
    app_notes VARCHAR(45) NULL,
    PRIMARY KEY (app_id),

    CONSTRAINT ux_app_ssn unique nonclustered (app_ssn ASC),
    CONSTRAINT ux_app_state_id unique nonclustered (app_state_id ASC)
);



----------------------------------------


