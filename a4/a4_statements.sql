--1.

drop view if exists v_user_info;
create view v_user_info as
select usr_id, usr_fname, usr_lname
CONCAT('(', substring(usr_phone,1,3),')', substring(usr_phone,4,3),'-', substring(usr_phone,7,4)) as usr_phone,
usr_email
from user
order by usr_lname asc;

select * from v_user_info;