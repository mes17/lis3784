use master;
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'mes17')
DROP DATABASE mes17;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'mes17')
CREATE DATABASE mes17;
GO

use mes17;
GO

IF OBJECT_ID (N'dbo.petstore',N'U') IS NOT NULL
DROP TABLE dbo.petstore;
GO

CREATE TABLE dbo.petstore
(
    pst_id TINYINT not null identity(1,1),
    pst_name VARCHAR(30) NOT NULL,
    pst_street VARCHAR(30) NOT NULL,
    pst_city VARCHAR(30) NOT NULL,
    pst_state CHAR(2) NOT NULL default 'AZ',
    pst_zip int NOT NULL check (pst_zip > 0 and pst_zip <= 999999999),
    pst_phone bigint NOT NULL,
    pst_email VARCHAR(100) NOT NULL,
    pst_url VARCHAR(100) NOT NULL,
    pst_ytd_sales DECIMAL(10,2) NOT NULL check (pst_ytd_sales >0),
    pst_notes VARCHAR(255) NULL,
    primary key(pst_id)
);

SELECT * from information_schema.tables;

insert into dbo.petstore
(pst_name,pst_street,pst_city,pst_state,pst_zip,pst_phone,pst_email,pst_url,pst_ytd_sales,pst_notes)
values
('Tom','1089 sw 66 st','Long Beach','CA','324509890','7777777777','tonygon@gmail.com',4000.00,.10,'loud'),
('Sophia','510 Daroco ave','Coral Gables','FL','673790675','9546113230','Emilyuck@hotmail.com',4000.00,.20,'cute'),
('Sara','6510 sw 10th st','Mexico City','MO','33134000','3059748140','Saranot@yahoo.com',6000.00,.30,'nun'),
('Timmy','6969 nw 666 st','Hell','MI','768921345','5183924612','Whother@aim.com',9000.00,.80,'Turn around'),
('Tyler','1800 Pennsylvania ave','Washington','DC','760934864','4046147728','look@me.com',2000.00,.20,'Winning');

select * from dbo.petstore;

IF OBJECT_ID (N'dbo.pet', N'U') IS NOT NULL
DROP TABLE dbo.pet;
GO

CREATE TABLE dbo.pet
(
    pet_id SMALLINT not null identity(1,1),
    pst_id TINYINT NULL,
    pet_type VARCHAR(45) NOT NULL,
    pet_sex CHAR(1) NOT NULL CHECK (pet_sex IN('m','f')),
    pet_cost DECIMAL(6,2) NOT NULL CHECK (pet_cost >0),
    pet_price DECIMAL(6,2) NOT NULL CHECK (pet_price >0),
    pet_age SMALLINT NOT NULL CHECK (pet_age >= 0 and pet_age <= 10500),
    pet_color VARCHAR(30) NOT NULL,
    pet_sale_date DATE NULL,
    pet_vaccine CHAR(1) NOT NULL CHECK (pet_vaccine IN('y','n')),
    pet_neuter CHAR(1) NOT NULL CHECK (pet_neuter IN('y','n')),
    pet_notes VARCHAR(255) NULL,
    primary key(pet_id),
    CONSTRAINT fk_pet_petstore
        FOREIGN KEY (pst_id)
        REFERENCES dbo.petstore (pst_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

SELECT * FROM information_schema.tables;

insert into dbo.pet
(pst_id,pet_type,pet_sex,pet_cost,pet_price,pet_age,pet_color,pet_vaccine,pet_neuter,pet_notes)
values
(1,'rabbit','f',300.00,320.00,2,'brown','y','y','wild'),
(2,'snake','m',700.00,710.00,6,'red','y','n','rare'),
(3,'wolf','m',500.00,940.00,4,'grey','n','n','fast'),
(4,'shark','m',200.00,520.00,2,'blue','y','y','gives fin'),
(5,'bird','f',600.00,230.00,10,'green','n','y','flies');

select * from dbo.pet;

EXEC sp_help 'dbo.pet';