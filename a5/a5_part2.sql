INSERT INTO dbo.room
(prp_id, rtp_id, rom_size, rom_notes) 

VALUES 
(1,1, '10" x 10"', NULL),
(3,2, '20" x 15"', NULL), 
(4,3, '8" x 8"', NULL),
(5,4, '50" x 50"', NULL),
(2,5, '30" x 30"', NULL);

INSERT INTO dbo.property
(prp_street, prp_city, prp_state, prp_zip, prp_type, prp_rental_rate, prp_status, prp_notes)

VALUES
('5133 3rd Road', 'Lake Worth', 'FL', '334671234', 'house', 1800.00, 'u', NULL), 
('92E Blah Way', 'Tallahassee', 'FL', '323011234', 'apt', 641.00, 'u', NULL),
('756 Diet Coke Lane','Panama City', 'FL', '342001234', 'condo', 2400.00, 'a', NULL),
('574 Doritos Circle','Jacksonville', 'FL', '365231234','townhouse', 1942.00, 'a', NULL),
('2241 W. Pensacola Street', 'Tallahassee', 'FL', '323041234', 'apt', 610.00, 'u', NULL);

INSERT INTO dbo.applicant
(app_ssn, app_state_id, app_fname, app_lname, app_street, app_city, app_state, app_zip, app_email, app_dob, app_gender, app_bckgd_check, app_notes)

VALUES
('123456789', 'A12C34S56Q78', 'Carla', 'Vanderbuilt', '5133 3rd Road', 'Lake Worth', 'FL', '334671234', 'csweeney@yahoo.com', '1961-11-26', 'F', 'y', NULL),
('590123454', 'B123A456D789', 'Amanda', 'Lindell', '2231 W. Pensacola Street', 'Tallahassee', 'FL', '323041234', 'acc10c@my.fsu.edu', '1981-04-04', 'F', 'y', NULL),
('987456321', 'dfed66532sedd', 'David', 'Stephens', '1293 Banana Code Drive', 'Panama City', 'FL', '323081234', 'mjowett@comcast.net', '1965-05-15', 'M', 'n', NULL),
('365214986', 'dgfg56597224', 'Chris', 'Thrombough', '987 Learning Drive', 'Tallahassee', 'FL', '323011234', 'landbeck@fsu.edu', '1969-07-25', 'M', 'y', NULL),
('326598236', 'yadayada4517', 'Spencer', 'Moore', '787 Tharpe Road', 'Tallahassee', 'FL', '323061234', 'spencer@my.fsu.edu', '1990-08-14', 'M', 'n', NULL);

INSERT INTO dbo.agreement
(prp_id, app_id, agr_signed, agr_start, agr_end, agr_amt, agr_notes)

VALUES
(3, 4, '2011-12-01', '2012-01-01', '2012-12-31', 1000.00, NULL),
(1, 1, '1983-01-01', '1983-01-01', '1987-12-31', 800.00, NULL),
(4, 2, '1999-12-31', '2000-01-01', '2004-12-31', 1200.00, NULL),
(5, 3, '1999-07-31', '1999-08-01', '2004-07-31', 750.00, NULL),
(2, 5, '2011-01-01', '2011-01-01', '2013-12-31', 900.00, NULL);

INSERT INTO dbo.occupant
(app_id, ocp_ssn, ocp_state_id, ocp_fname, ocp_lname, ocp_email, ocp_dob, ocp_gender, ocp_bckgd_check, ocp_notes)

VALUES
(1, '326532165', 'okd557ig4125', 'Bridget', 'Case-Sweeney', 'bcs10c@gmail.com', '1988-03-23', 'F', 'y', NULL),
(1, '187452457', 'uhtooold', 'Brian', 'Sweeney', 'brian@sweeney.com', '1956-07-28', 'M', 'y', NULL),
(2, '123456780', 'thisisdoggie', 'Skittles', 'McGoobs', 'skittles@wags.com', '2011-01-01', 'F', 'n', NULL),
(2, '098123664', 'thisiskitty', 'Smalls', 'Balls', 'smalls@meanie.com', '1988-03-05', 'M', 'n', NULL),
(5, '857694032', 'thisisbaby324', 'Baby', 'Girl', 'baby@girl.com', '2013-04-08', 'F', 'n', NULL);