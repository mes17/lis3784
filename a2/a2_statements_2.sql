-- 1. List all faculty members’ first and last names, full addresses, salaries, and hire dates. (c,d)

-- old style join

select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date
from person as p, employee as e, faculty as f 
where p.per_id=e.per_id and e.per_id=f.per_id;

--  join on

select p.per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date 
from person p 
join employee e on p.per_id=e.per_id 
join faculty f on e.per_id=f.per_id;

--  join using

select per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date 
from person 
join employee using (per_id) 
join faculty using (per_id);

--  natural join

select per_id, per_fname, per_lname, per_street, per_city, per_state, emp_salary, fac_start_date 
from person 
natural join employee 
natural join faculty;


-- 2. List the first 10 alumni’s names, genders, date of births, degree types, areas, and dates. 

-- old style join
select p.per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date 
from person p, alumnus a, degree d 
where p.per_id = a.per_id 
and a.per_id = d.per_id 
limit 0, 10;

-- join on
select p.per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
from person p 
join alumus a on p.per_id = a.per_id 
join degree d on a.per_id = d.per_id
limit 0, 10;

-- join using
select per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date
from person 
join alumnus using(per_id)
join degree using(per_id)
limit 0, 10;

-- natural join 
select per_id, per_fname, per_lname, per_gender, per_dob, deg_type, deg_area, deg_date 
from person 
natural join alumnus
natural join degree 
limit 0, 10;


-- 3. List the last 20 undergraduate names, majors, tests, scores, and standings. 

-- old style join
select p.per_id, per_fname, per_lname, stu_major, ugd_test, ugd_score, ugd_standing
from person p, student s, undergrad u 
where p.per_id = s.per_id 
and s.per_id = u.per_id 
order by per_id desc 
limit 0, 20;

-- join using
select per_id, per_fname, per_lname, stu_major, ugd_test, ugd_score, ugd_standing
from person 
join student using (per_id) 
join undergrad

-- 4. Remove the first 10 staff members; after which, display the remaining staff members’ names and positions. 

delete from staff 
order by per_id limit 10;

-- 5. Update one graduate student’s test score (only one score) by 10%. Display the before and after values to verify that it was updated. 

select * from grad;

update grad
set grd_score = grd_score * 1.10
where per_id = 75 and grd_test = 'gmat';

select * from grad;

-- 6. Add two new alumni, using only one SQL statement (include attributes). Then, verify that two records have been added. 

select * from alumus;

insert into alumus
(per_id, alm_notes)
values 
(97, "testing1"),
(98, "testing2");

select * from alumus;

