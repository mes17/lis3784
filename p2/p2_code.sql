SET ANSI_WARNINGS ON;
GO

use master;
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name =N'mes17')
DROP DATABASE mes17;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name =N'mes17')
CREATE DATABASE mes17;
GO

use mes17;
GO

--------------------------start of tables-----------------------

IF OBJECT_ID (N'dbo.patient',N'U') IS NOT NULL
DROP TABLE dbo.patient;
GO

CREATE TABLE dbo.patient
(
    pat_id SMALLINT not null identity(1,1),
    pat_ssn int NOT NULL check (pat_ssn > 0 and pat_ssn <= 999999999),
    pat_fname VARCHAR(15) NOT NULL,
    pat_lname VARCHAR(30) NOT NULL,
    pat_street VARCHAR(30) NOT NULL,
    pat_city VARCHAR(30) NOT NULL,
    pat_state CHAR(2) NOT NULL DEFAULT 'FL',
    pat_zip int NOT NULL check (pat_zip > 0 and pat_zip <= 999999999),
    pat_phone BIGINT NOT NULL CHECK (pat_phone > 0 and pat_phone <= 9999999999),
    pat_email VARCHAR(100) NULL,
    pat_dob DATE NOT NULL,
    pat_gender CHAR(1) NOT NULL CHECK (pat_gender IN('m','f')),
    pat_notes VARCHAR(45) NULL,
    PRIMARY KEY (pat_id),

    CONSTRAINT ux_pat_ssn unique nonclustered (pat_ssn ASC),
);

-------------Medication---------------------------


IF OBJECT_ID (N'dbo.medication',N'U') IS NOT NULL
DROP TABLE dbo.medication;

CREATE TABLE dbo.medication
(
    med_id SMALLINT NOT NULL identity(1,1),
    med_name VARCHAR(100) NOT NULL,
    med_price DECIMAL(5,2) NOT NULL CHECK (med_price > 0),
    med_shelf_life DATE NOT NULL,
    med_notes VARCHAR(255) NULL,
    PRIMARY KEY (med_id)
);

------------prescription--------------------

IF OBJECT_ID (N'dbo.prescription',N'U') IS NOT NULL
DROP TABLE dbo.prescription;

CREATE TABLE dbo.prescription
(
    pre_id SMALLINT NOT NULL identity(1,1),
    pat_id SMALLINT NOT NULL,
    med_id SMALLINT NOT NULL,
    pre_date DATE NOT NULL,
    pre_dosage VARCHAR(255) NOT NULL,
    pre_num_refills VARCHAR(3) NOT NULL,
    pre_notes VARCHAR(255) NULL,
    PRIMARY KEY (pre_id),

    CONSTRAINT ux_pat_id_med_id_pre_date unique nonclustered
    (pat_id ASC, med_id ASC, pre_date ASC),

    CONSTRAINT fk_prescription_patient
        FOREIGN KEY (pat_id)
        REFERENCES dbo.patient (pat_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_prescription_medication
        FOREIGN KEY (med_id)
        REFERENCES dbo.medication (med_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,
);

---------------------treatment---------------------------------------

IF OBJECT_ID (N'dbo.treatment',N'U') IS NOT NULL
DROP TABLE dbo.treatment;

CREATE TABLE dbo.treatment
(
    trt_id smallint NOT NULL identity(1,1),
    trt_name VARCHAR(255) NOT NULL,
    trt_price DECIMAL(8,2) NOT NULL CHECK (trt_price > 0),
    trt_notes VARCHAR(255) NULL,
    PRIMARY KEY (trt_id)
);

------------physician-------------

IF OBJECT_ID (N'dbo.physician',N'U') IS NOT NULL
DROP TABLE dbo.physician;

CREATE TABLE dbo.physician
(
    phy_id SMALLINT NOT NULL identity(1,1),
    phy_specialty VARCHAR(25) NOT NULL,
    phy_fname VARCHAR(15) NOT NULL,
    phy_lname VARCHAR(30) NOT NULL,
    phy_street VARCHAR(30) NOT NULL,
    phy_city VARCHAR(30) NULL,
    phy_state CHAR(2) NOT NULL DEFAULT 'FL',
    phy_zip int NOT NULL check (phy_zip > 0 and phy_zip <=999999999),
    phy_phone BIGINT NOT NULL CHECK (phy_phone > 0 and phy_phone <= 9999999999),
    phy_fax BIGINT NOT NULL CHECK (phy_fax > 0 and phy_fax <= 9999999999),
    phy_email VARCHAR(100) NULL,
    phy_url VARCHAR(100) NULL,
    phy_notes VARCHAR(45) NULL,
    PRIMARY KEY (phy_id),  
);

---------------------patient_treatment--------------------

IF OBJECT_ID (N'dbo.patient_treatment',N'U') IS NOT NULL
DROP TABLE dbo.patient_treatment;

CREATE TABLE dbo.patient_treatment
(
    ptr_id SMALLINT NOT NULL identity(1,1),
    pat_id SMALLINT NULL,
    phy_id SMALLINT NULL,
    trt_id SMALLINT NULL,
    ptr_date DATE NOT NULL,
    ptr_start TIME(0) NOT NULL,
    ptr_end TIME(0) NOT NULL,
    ptr_results VARCHAR(255) NULL,
    ptr_notes VARCHAR(255) NULL,
    PRIMARY KEY (ptr_id),

    CONSTRAINT ux_pat_id_phy_id_trt_id_ptr_date unique nonclustered (pat_id ASC, phy_id ASC, trt_id ASC, ptr_Date ASC),

    CONSTRAINT fk_patient_treatment_patient
        FOREIGN KEY (pat_id)
        REFERENCES dbo.patient (pat_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_patient_treatment_physician
        FOREIGN KEY (phy_id)
        REFERENCES dbo.physician (phy_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_patient_treatment_treatment
        FOREIGN KEY (trt_id)
        REFERENCES dbo.treatment (trt_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,
);


-----------------------administration_lu-----------------

IF OBJECT_ID (N'dbo.administration_lu',N'U') IS NOT NULL
DROP TABLE dbo.administration_lu;

CREATE TABLE dbo.administration_lu
(
    pre_id SMALLINT NOT NULL,
    ptr_id SMALLINT NOT NULL,
    PRIMARY KEY (pre_id, ptr_id),

    CONSTRAINT fk_administration_lu_prescription
        FOREIGN KEY (pre_id)
        REFERENCES dbo.prescription (pre_id)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,

    CONSTRAINT fk_administration_lu_patient_treatment
        FOREIGN KEY (ptr_id)
        REFERENCES dbo.patient_treatment (ptr_id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,

);

------------------show tables-------------

SELECT * FROM information_schema.tables;

EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

-----------patient table--------------

INSERT INTO dbo.patient
(pat_ssn, pat_fname, pat_lname, pat_street, pat_city, pat_state, pat_zip, pat_phone, pat_email, pat_dob, pat_gender, pat_notes)

VALUES
('123456789', 'Carla', 'Vanderbilt', '5133 3rd Road', 'Lake Worth', 'FL', '334671234', 5674892390, 'csweeney@yahoo.com', '11-26-1961', 'F', NULL),
('590123654', 'Amanda', 'Lindell', '2241 W. Pensacola Street', 'Tallahassee', 'FL', '323041234', 9876543210, 'acm10c@my.fsu.edu', '04-04-1981', 'F', NULL),
('987456321', 'David', 'Stephens', '1293 Banana Code Drive', 'Panama City', 'FL', '323081234', 8507632145, 'dstephens@comcast.com', '05-15-1965', 'M', NULL),
('365214986', 'Chris', 'Thrombough', '987 Learning Drive', 'Tallahassee', 'FL', '323011234', 5768941254, 'cthrombough@fsu.edu', '07-25-1969', 'M', NULL),
('326598236', 'Spencer', 'Moore', '787 Tharpe Road', 'Tallahassee', 'FL', '323061234', 8764930213, 'spencer@my.fsu.edu', '08-14-1990', 'M', NULL);

------------medication table-----------

INSERT INTO dbo.medication
(med_name, med_price, med_shelf_life, med_notes)

VALUES
('Abilify',200.00, '06-23-2014', NULL),
('Aciphex',125.00, '06-24-2014', NULL),
('Actonel',250.00, '06-25-2014', NULL),
('Actoplus MET',412.00, '06-26-2014', NULL),
('Actos',89.00, '06-27-2014', NULL),
('Adacel',66.00, '06-28-2014', NULL),
('Adderall XR',69.00, '06-29-2014', NULL),
('Advair Diskus',45.00, '06-30-2014', NULL),
('Aggrenox',66.99, '07-01-2014', NULL),
('Aloxi',145.00, '07-02-2014', NULL);

-------------------prescription table-------------

INSERT INTO dbo.prescription
(pat_id, med_id, pre_date, pre_dosage, pre_num_refills,pre_notes)

VALUES
(1,1,'2011-12-23', 'take one per day', '1', NULL),
(1,2,'2011-12-24', 'take as needed', '2',  NULL),
(2,3,'2011-12-25', 'take two before and after dinner', '1',  NULL),
(2,4,'2011-12-26', 'take one per day', '2',  NULL),
(3,5,'2011-12-27', 'take one per day', '1', NULL),
(3,6,'2011-12-28', 'take two before and after dinner', '2', NULL),
(4,7,'2011-12-29', 'take one per day', '1', NULL),
(4,8,'2011-12-30', 'take as needed', '2', NULL),
(5,9,'2011-12-31', 'take two before and after dinner', '1', NULL),
(5,10,'2012-01-01', 'take one per day', 'rpn', NULL);


-----------------physician table---------------


INSERT INTO dbo.physician
(phy_specialty, phy_fname, phy_lname, phy_street, phy_city, phy_state, phy_zip, phy_phone, phy_fax, phy_email, phy_url, phy_notes)

VALUES
('family medicine', 'tom','smith','987 peach st','tampa','FL','33610','9876541245','998854545','tsmith@gmail.com','tsmithfamilymed.com', NULL),
('internal medicine', 'steve','williams','963 plum lane','miami','FL','36341','9656547412','8544742121','swilliams@gmail.com','swilliamsmedicine.com', NULL),
('pediatrician', 'ronald','burns','645 wave circle','orlando','FL','33214','9657894565','8457411147','rburns@gmail.com','rburnspeditrics.com', NULL),
('psychiatrist', 'pete','roger','1233 stadium lane','orlando','FL','33214','7418529999','3213216565','peteroger@gmail.com','progerpysch.com', NULL),
('dermatology', 'dave','roger','654 hard drive','miami','FL','36631','9876547412','6546565541','droger@gmail.com','drogerderma.com', NULL),
('anesthesiology', 'mike','jordan','65464 stadium dr','tallahassee','FL','36631','6546544545','9639633699','mjordan@gmail.com','mjordan.com', NULL),
('cardiovascular surgery', 'ronald','reagan','987 biscayne bay','miami','FL','36631','3057895412','8528527411','rreagan@gmail.com','rreagancardio.com', NULL),
('cancer surgery', 'penny','hardaway','8798 collins ave','miami','FL','36631','3054441414','9876542222','phardaway@gmail.com','phardawaysurgery.com', NULL),
('gynecology', 'kevin','durant','9876 washington ave','miami','FL','36631','3057414125','7897894444','kdurant@gmail.com','kdurantgyn.com', NULL),
('gastroenterology', 'pat','thomas','3243 jefferson st','tampa','FL','36631','8135412541','5554446565','pthomas@gmail.com','ptomasgas.com', NULL);

----------------treatment table-----------------

INSERT INTO dbo.treatment
(trt_name, trt_price, trt_notes)

VALUES
('knee replacement',2000.00,NULL),
('heart transplant',130000.00,NULL), 
('hip replacement',4000.00,NULL), 
('tonsils removed',5000.00,NULL), 
('skin graft',2000.00,NULL), 
('breast enchancement',20000.00,NULL), 
('septrohinoplasty',60000.00,NULL), 
('kindney transplant',12000.00,NULL), 
('shoulder surgery',18000.00,NULL), 
('appendix removal',3000.00,NULL);

--------------------patient_treatment------------------------

INSERT INTO dbo.patient_treatment
(pat_id, phy_id, trt_id, ptr_date, ptr_start, ptr_end, ptr_results, ptr_notes)

VALUES
(1,10,5,'2011-12-23','07:08:09','10:12:15','success patient is fine', NULL),
(1,9,6,'2011-12-24','08:08:09','11:12:15','complications patient will repeat procedure at a later time', NULL),
(2,8,7,'2011-12-25','09:08:09','12:12:15','died during surgery', NULL),
(2,7,8,'2011-12-26','10:08:09','13:12:15','success patient is fine', NULL),
(2,6,9,'2011-12-27','11:08:09','14:12:15','complications patient will repeat procedure at a later time', NULL),
(3,5,10,'2011-12-28','12:08:09','15:12:15','died during surgery', NULL),
(3,4,1,'2011-12-29','13:08:09','16:12:15','success patient is fine', NULL),
(4,3,2,'2011-12-30','14:08:09','17:12:15','complications patient will repeat procedure at a later time', NULL),
(4,2,3,'2011-12-31','15:08:09','18:12:15','removed wrong one', NULL),
(5,1,4,'2012-01-01','16:08:09','19:12:15','success patient is fine', NULL);

-------------------administration_lu------------

INSERT INTO dbo.administration_lu
(pre_id,ptr_id)

VALUES(10,5),(9,6),(8,7),(7,8),(6,9),(5,10),(4,9),(3,8),(2,7),(1,6);

---------------------------end--------------------------

exec sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"

select * from dbo.patient;
select * from dbo.medication;
select * from dbo.prescription;
select * from dbo.physician;
select * from dbo.treatment;
select * from dbo.patient_treatment;
select * from dbo.administration_lu;